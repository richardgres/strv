$(function () {

    var body = $('body');
    var mobileMenu = $("#mobile-menu");
    var visual = $("#visual");

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 100) {
            mobileMenu.addClass("scroll");
        } else {
            mobileMenu.removeClass("scroll");
        }
    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 300) {
            body.addClass('scrolled');
        } else {
            body.removeClass('scrolled');
        }
    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 1400) {
            visual.css({"left":"-50px"});
        }
    });

    $("#toggle-menu").click(function () {
        $(this).parent().removeClass('scroll').toggleClass('open');
        return false;
    });

    $("a.toggle-mobile").click(function () {
        $('#mobile-menu').removeClass('open');
        return false;
    });


    $('a[href*=#]:not([href=#])').click(function() {

        $(".main-menu li").removeClass("scroll");
        $(this).closest('li').addClass('active');

        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('body').animate({
                    scrollTop: target.offset().top
                }, 2000);
                return false;
            }
        }

    });

});
